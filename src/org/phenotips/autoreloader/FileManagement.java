package org.phenotips.autoreloader;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedList;
import java.util.List;

/**
 * Watches for file changes, builds maven modules, and copies them over.
 */
public class FileManagement
{
    private List<String> reloadList = new LinkedList<>();

    private Autoreloader.ReDeployer reloadJar;

    public FileManagement(String lib, String toWatch, Autoreloader.ReDeployer reloadJar) throws Exception
    {
        this.reloadJar = reloadJar;
        Path pathToWatch = new File(toWatch).toPath();
        Path libPath = new File(lib).toPath();
        DirectoryWatcher dirWatcher = new DirectoryWatcher(libPath, pathToWatch, this);
        Thread dirWatcherThread = new Thread(dirWatcher);
        dirWatcherThread.start();
    }

    // synchronizing on the class instance
//    /** Clears the reload list. */
    public synchronized List<String> getReloadList()
    {
        List<String> list = reloadList;
//        reloadList = new LinkedList<>();
        return list;
    }

    public synchronized void addToReloadList(String classPath)
    {
        System.out.println("\nAdding to reload list " + classPath);
        String className = classPath.replace(".java", "").replaceAll("/", ".");
        reloadList.add(className);
//        System.out.println("Reloading the jar loader.");
        this.reloadJar.redeploy(className);
    }
}

class DirectoryWatcher implements Runnable
{
    private Runtime runtime = Runtime.getRuntime();

    private Path libPath;

    private Path watchPath;

    private FileManagement fileManagement;

    public DirectoryWatcher(Path libPath, Path watchPath, FileManagement management)
    {
        this.watchPath = watchPath;
        this.libPath = libPath;
        this.fileManagement = management;
    }

    // print the events and the affected file
    private void processEvent(WatchKey watchKey, WatchEvent<?> event)
    {
        Path changeParentPath = (Path) watchKey.watchable();
        Path changePath = changeParentPath.resolve((Path) event.context());
        if (changeParentPath.endsWith("target") && changePath.endsWith("target")) {
            return;
        }
        File pomDir = findPomDir(changeParentPath.toFile());
        System.out.println("Detected change in " + changeParentPath.toString() + " pom in" + pomDir.toString());

        try {
//            String buildCommand = String.format("cd %s && mvn clean install", pomDir.toString());
            String buildCommand =
                String.format("mvn -f %s -Pquick clean install", pomDir.getAbsolutePath() + "/pom.xml");
            String targetDirString = String.format("%s/target/", pomDir.getAbsolutePath());

            Process build = runtime.exec(buildCommand);
            build.waitFor();

            File[] targetJars = new File(targetDirString).listFiles(new FilenameFilter()
            {
                @Override public boolean accept(File dir, String name)
                {
                    return name.endsWith("jar");
                }
            });
            for (File jar : targetJars) {
                Files.copy(jar.toPath(), libPath.toAbsolutePath().resolve(jar.getName()),
                    StandardCopyOption.REPLACE_EXISTING);
            }

            if (targetJars.length > 0) {
                System.out.println("Updated " + pomDir.toString());
                String changePathString = changePath.toString();
                String srcDelimiter = "src/main/java/";
                int srcFolderPosition = changePathString.indexOf(srcDelimiter) + srcDelimiter.length();
                this.fileManagement.addToReloadList(changePathString.substring(srcFolderPosition));
            } else {
                System.out.println("No jar copied " + pomDir.toString());
            }
        } catch (InterruptedException | IOException ex) {
            System.out.println("Could not build module.");
        }
    }

    private File findPomDir(File currentDir)
    {
        File[] pom = currentDir.listFiles(new FilenameFilter()
        {
            @Override public boolean accept(File dir, String name)
            {
                return name.equals("pom.xml");
            }
        });
        if (pom.length > 0) {
            return currentDir;
        } else {
            return findPomDir(currentDir.getParentFile());
        }
    }

    @Override
    public void run()
    {
        try {
            WatchService watchService = watchPath.getFileSystem().newWatchService();
            registerAll(this.watchPath.toAbsolutePath(), watchService);

            // loop forever to watch directory
            while (true) {
                WatchKey watchKey;
                watchKey = watchService.take(); // this call is blocking until events are present

                // poll for file system events on the WatchKey
                for (final WatchEvent<?> event : watchKey.pollEvents()) {
                    processEvent(watchKey, event);
                }

                // if the watched directed gets deleted, get out of run method
                if (!watchKey.reset()) {
                    System.out.println("No longer valid " + watchKey.watchable());
                }
//                Thread.sleep(1000 * 60);
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            System.out.println("interrupted. Goodbye");
            return;
        } catch (IOException ex) {
            ex.printStackTrace();  // don't do this in production code. Use a loggin framework
            return;
        }
    }

    private void registerAll(final Path start, final WatchService watcher) throws IOException
    {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>()
        {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                throws IOException
            {
                System.out.println("Start " + start.toString());
                if (!dir.getFileName().endsWith("target") &&
                    !dir.getFileName().endsWith("resources") &&
                    !dir.getFileName().endsWith(".git") && !dir.getFileName().endsWith(".idea"))
                {
                    System.out.println("\t\t" + dir.resolve("target"));
                    if (!dir.resolve("target").toFile().exists() && dir != start && dir.getParent() != start) {
//                        System.out.println("Registering dir " + dir.toString());
                        dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
                            StandardWatchEventKinds.ENTRY_DELETE,
                            StandardWatchEventKinds.ENTRY_MODIFY);
                    }
                    return FileVisitResult.CONTINUE;
                } else {
                    return FileVisitResult.SKIP_SUBTREE;
                }
            }
        });
    }
}
