package org.phenotips.autoreloader;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.eclipse.jetty.webapp.WebAppClassLoader;
import org.eclipse.jetty.webapp.WebAppContext;

public class Autoreloader extends WebAppClassLoader
{
//    private WebAppContext webContext;

    private final File jarFolder = new File("webapps/phenotips/WEB-INF/lib");

    private FileManagement fileManagement;

    private URLClassReloader jarLoader;

    private String watchPath;

    public static void main(String[] args)
    {
    }

    public Autoreloader(String watchPath) throws IOException
    {
        super(new WebAppContext("/webapps/phenotips", "/"));
        this.watchPath = watchPath;
        System.out.println(watchPath);
        System.out.println("Current directory is " + System.getProperty("user.dir"));

        try {
            loadJarLoader();

            // Watching changes
            fileManagement = new FileManagement(jarFolder.toString(), watchPath, new ReDeployer(this));
        } catch (Exception ex) {
            System.out.println("Could not instantiate the autoreloader. This is critical.");
            ex.printStackTrace();
        }
    }

    class ReDeployer
    {
        private Autoreloader autoreloader;

        public ReDeployer(Autoreloader reloader)
        {
            this.autoreloader = reloader;
        }

        public void redeploy(String classToDeploy)
        {
            try {
                String className = classToDeploy.replace("/", ".");
                System.out.println("Redeploying " + className);
                System.out.println("Current classloader " + jarLoader.toString());
//                this.autoreloader.loadClass(className);
//                loadJarLoader();
                autoreloader.loadClass(className);
                System.out.println("New classloader " + jarLoader.toString());
//            } catch (IOException|ClassNotFoundException ex) {
            } catch (ClassNotFoundException ex) {
                System.out.println("Did not find the class to redeploy.");
            }
        }
    }

    protected void loadJarLoader() throws IOException
    {
        jarLoader = new URLClassReloader(getContext(), this.getParent());
        ((WebAppContext) getContext()).setClassLoader(jarLoader);
    }

    public Autoreloader(WebAppContext context) throws IOException
    {
        super(context);
    }

    public Autoreloader(ClassLoader parent, WebAppContext context) throws IOException
    {
        super(parent, context);
    }
//
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException
    {
//        System.out.println("Loading class :" + name);
////        Class requestedClass = null;
//        try {
//            return super.loadClass(name);
////            System.out.println("SUPER loader found");
//        } catch (ClassNotFoundException notFound) {
//            System.out.println("SUPER loader NOT found");
//            throw notFound;
//        }
////        System.out.println("---\n");
        if (jarLoader.getURLs().length == 0) {
            for (URL url : this.getURLs()) {
//                System.out.println("Adding url " + url);
                jarLoader.addURL(url);
            }
        }
//        try {
//            return jarLoader.loadClass(name);
//        } catch (Exception ex) {
            return this.loadClass(name, false);
//        }
    }

    @Override
    protected synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException
    {
//        if (name.startsWith("org.phenotips")) {
            try {
                System.out.println(name);
                return jarLoader.loadClass(name, resolve);
            } catch (Exception ex) {
                System.out.println("\tMiss");
                throw ex;
            }
//        }

//        Class<?> c = findLoadedClass(name);
////        Class<?> c = null;
//        ClassNotFoundException ex = null;
//        boolean tried_parent = false;
//
//        boolean system_class = super.getContext().isSystemClass(name);
//        boolean server_class = super.getContext().isServerClass(name);
//
//        if (system_class && server_class) {
//            return null;
//        }
//
//        if (c == null && super.getParent() != null && (super.getContext().isParentLoaderPriority() || system_class) &&
//            !server_class)
//        {
//            tried_parent = true;
//            try {
//                c = super.getParent().loadClass(name);
//                System.out.println("loaded " + c);
//            } catch (ClassNotFoundException e) {
//                ex = e;
//            }
//        }
//
//        if (c == null) {
//            try {
//                c = this.findClass(name);
//            } catch (ClassNotFoundException e) {
//                ex = e;
//            }
//        }
//
//        if (c == null && super.getParent() != null && !tried_parent && !server_class) {
//            c = super.getParent().loadClass(name);
//        }
//
//        if (c == null) {
//            throw ex;
//        }
//
//        if (resolve) {
//            resolveClass(c);
//        }
//
//        System.out.println("loaded " + c + " from " + c.getClassLoader());
//
//        return c;
    }
}

//
//// Watching changes
//fileManagement = new FileManagement(jarFolder.toString(), watchPath, new Runnable()
//    {
//@Override public void run()
//    {
//    try {
////                        loadJarLoader(jarFolder);
////                        System.gc();
//    } catch (MalformedURLException ex) {
//    System.out.println("Could not reload the jar loader");
//    }
//    }
//    }, jarLoader);
