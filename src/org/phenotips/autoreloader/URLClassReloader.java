package org.phenotips.autoreloader;

import java.io.IOException;
import java.net.URL;

import org.eclipse.jetty.webapp.WebAppClassLoader;

/**
 * A hacked version of a URL class loader.
// */
public class URLClassReloader extends WebAppClassLoader
{
    private ClassLoader parentLoader;

    public URLClassReloader(WebAppClassLoader.Context context, ClassLoader parentLoader) throws IOException
    {
        super(parentLoader, context);
        this.parentLoader = parentLoader;
    }

    @Override public synchronized Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException
    {
        return super.loadClass(name, resolve);
    }

    @Override protected void addURL(URL url)
    {
        super.addURL(url);
    }

    //    @Override public Class<?> loadClass(String name) throws ClassNotFoundException
//    {
//        try {
//            return parentLoader.loadClass(name);
//        } catch (Exception ex) {
//            Class<?> c;
//            try {
//                c = super.loadClass(name);
//            } catch (Exception ex2) {
//                throw ex2;
//            }
//            return c;
//        }
//    }
}
